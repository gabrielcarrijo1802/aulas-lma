//Processamento dos dados do formulário "index.html"

//Acessando os elementos do formulário do html
const formulario = document.getElementById("formulario1");

//aciona um ouvinte de eventos a um elemento HTML
formulario.addEventListener("submit", function (evento) 
{
    evento.preventDefault();
    /*previne o comportamento padrão de um elemento HTML em resposta a um evento*/

  //constantes para tratar os dados recebidos dos elementos do formulário
  const nome = document.getElementById("nome").value;
  const email = document.getElementById("email").value;

  //Exibe um alerta com os dados coletados
  alert(`Nome: ${nome} --- E-mail: ${email} `);

  const updateResultado = document.getElementById("resultado");

  updateResultado.value = `Nome: ${nome} ------ E-mail: ${email}`;

  updateResultado.style.width = updateResultado.scrollWidth + "px";


});