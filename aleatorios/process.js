const{createApp} = Vue;
createApp({
    data(){
        return{
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[
                './imagens/lua.jpg',
                './imagens/SENAI_logo.png',
                './imagens/sol.jpg'
            ],

            imagensInternet:[
                'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg',
                'https://www.lance.com.br/files/article_main/uploads/2023/03/09/640a94d6c9d66.jpeg',
                'https://maisqueumjogo.com.br/wp-content/uploads/2022/12/Rafael-Leao-1-scaled.jpg'
            ],

        }//Fim return
    },//Fim data

    computed:{
        randomImage(){
            return this.imagensLocais[this.randomIndex];
        },//Fim randomImage

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];
        }//Fim randomImageInternet
    },//Fim computed

    methods:{
        getRandomImage()
        {
            this.randomIndex=Math.floor(Math.random()*this.imagensLocais.length);

            this.randomIndexInternet=Math.floor(Math.random()*this.imagensInternet.length); 
        }
    },//Fim methods
    
}).mount("#app");