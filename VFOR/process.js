const {createApp} = Vue;

createApp({
    data(){
        return{
            show: false,
            itens:["Bola", "Bolsa", "Tenis"],
        };
    },//F. data

    methods:{
        showItens:function(){
            this.show = !this.show;
        },//F. showItens
    },//F. methods

}).mount("#app");//F. createApp